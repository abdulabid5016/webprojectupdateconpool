<%@page import="java.io.PrintWriter"%>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>Login Page</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style type="text/css">
.bs-example {
	margin: 20px;
}

.sidenav {
	height: 100%;
	width: 160px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	padding-top: 20px;
}

.sidenav a {
	padding: 6px 8px 6px 16px;
	text-decoration: none;
	font-size: 25px;
	color: #818181;
	display: block;
}

.sidenav a:hover {
	color: #f1f1f1;
}

.main {
	margin-left: 160px; /* Same as the width of the sidenav */
	font-size: 28px; /* Increased text to enable scrolling */
	padding: 0px 10px;
}

@media screen and (max-height: 450px) {
	.sidenav {
		padding-top: 15px;
	}
	.sidenav a {
		font-size: 18px;
	}
}
</style>

</head>

<body>
	<div class="bs-example">
		<nav class="navbar navbar-expand-md navbar-light bg-light">
			<a href="#" class="navbar-brand"> <img src="11.png" height="42"
				alt="CoolBrand">
			</a>
			<button type="button" class="navbar-toggler" data-toggle="collapse"
				data-target="#navbarCollapse">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarCollapse">
				<div class="navbar-nav"></div>

				<div class="navbar-nav ml-auto">
					<a href="#" class="nav-item nav-link">Welcome <%String user1=(String)session.getAttribute("user"); %>
						<%out.print(user1);  %> <%--please be careful once use logout  you cannot move back again --%>
					</a> <a href="Logout.jsp" class="nav-item nav-link">Logout </a>
				</div>
			</div>
		</nav>
		<div class="sidenav"
			style="margin-top: 81px;
	/* padding-top: 48px; */ margin-left: 16px;">
			<a href="about.jsp">about</a> <a href="products.jsp">products</a> <a
				href="roles.jsp">roles</a> <a href="user.jsp">user</a>

		</div>
</body>
</html>
