package com.Servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.postgresql.util.PSQLException;

import com.assessment.service.RegisterService;
import com.assessment.service.impl.RegisterServiceImpl;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	  @Resource(name="jdbc/WebApplication")
      private DataSource datasource;
     

      private RegisterService registerService = new RegisterServiceImpl();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		boolean success;
		try {
			success = registerService.register(request, response);
			if (success) {
				response.sendRedirect("login.jsp");
			}
			else {
				response.sendRedirect("email.jsp");
			}
		} catch (PSQLException e) {
			// TODO Auto-generated catch block
			
		}
			
		
		
	}
}

