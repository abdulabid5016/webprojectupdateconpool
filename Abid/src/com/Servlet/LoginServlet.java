package com.Servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.assessment.dao.LoginDao;
import com.assessment.dao.LoginSession;
import com.assessment.dao.impl.LoginDaoImpl;
import com.assessment.dao.impl.LoginSessionImpl;
import com.assessment.service.LoginService;
import com.assessment.service.impl.LoginServiceImpl;
import com.assessments.models.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/WebApplication")
private DataSource datasource;
private LoginService loginService = new LoginServiceImpl();
private LoginDao logindao=new LoginDaoImpl();
User user=new User();


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		user=loginService.Login(request, response);
	
	boolean a=logindao.Login(user);
		if(a)
		{
		 LoginSession loginsession=new LoginSessionImpl();

			String name=loginsession.LoginSession1(user.getEmail());
			System.out.println("name");
			HttpSession session=request.getSession();
			session.setAttribute("user", name);
			System.out.println(name);
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("homepage.jsp");
			requestDispatcher.forward(request, response);
		}
		else
		{
			
			RequestDispatcher rd=request.getRequestDispatcher("error.jsp"); 
            rd.include(request, response);
		}
	
	
	
	
	
	}

}
