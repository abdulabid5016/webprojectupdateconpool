package com.assessment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;

import org.postgresql.util.PSQLException;

import com.assessment.dao.RegistrationDao;
import com.assessments.models.User;

import DBUtil.DBUtil;


public class RegistrationDaoimpl implements RegistrationDao{
	//@Resource(name="jdbc/WebApplication")

	Connection myConn=null;
	Statement mystmt=null;
	ResultSet myRs=null;
 public RegistrationDaoimpl() 
	 {
		 try {
			myConn=DBUtil.getDataSource().getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}

//	}


	@Override
	public boolean register1(User user) throws PSQLException{

	String email=user.getEmail();
	String name=user.getName();
	String password=user.getPassword();
	String mobile=user.getMobile();

		try {
			String sql="insert into users values(?,?,?,?)";
			myConn=DBUtil.getDataSource().getConnection();
				PreparedStatement ps = myConn.prepareStatement(sql);

				ps.setString(1, email);
				ps.setString(2, name);
				ps.setString(3, password);
				ps.setString(4, mobile);
			 ps.execute();
			 return true;
		} 
		catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		
}

	@Override
	public boolean register(User user) {
		System.out.println("Hello in dao");
		return false;
	}
	
}
