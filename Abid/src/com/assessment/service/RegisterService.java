package com.assessment.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.postgresql.util.PSQLException;

public interface RegisterService {

	boolean register(HttpServletRequest req, HttpServletResponse resp) throws PSQLException;

}
